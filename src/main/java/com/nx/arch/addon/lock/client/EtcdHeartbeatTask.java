package com.nx.arch.addon.lock.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @类名称 EtcdHeartbeatTask.java
 * @类描述 etcd 心跳任务：用于节点探活，节点重入
 * @作者  庄梦蝶殇 linhuaichuan@naixuejiaoyu.com
 * @创建时间 2020年3月28日 下午2:57:30
 * @版本 1.0.0
 *
 * @修改记录
 * <pre>
 *     版本                       修改人 		修改日期 		 修改内容描述
 *     ----------------------------------------------
 *     1.0.0 		庄梦蝶殇 	2020年3月28日             
 *     ----------------------------------------------
 * </pre>
 */
class EtcdHeartbeatTask extends Thread {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(EtcdHeartbeatTask.class);
    
    private final EtcdClient etcdClient;
    
    EtcdHeartbeatTask(EtcdClient etcdClient) {
        this.etcdClient = etcdClient;
    }
    
    @Override
    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(10);
                testAvailableEtcdNodes();
                testBrokenEtcdNodes();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * @方法名称 testAvailableEtcdNodes
     * @功能描述 节点探活
     */
    private void testAvailableEtcdNodes() {
        List<String> availableEtcdNodes = new ArrayList<String>(etcdClient.getAvailableEtcdNodes());
        for (String etcdNode : availableEtcdNodes) {
            try {
                URI uri = new URI(etcdNode);
                testSocketConnect(uri.getHost(), uri.getPort());
            } catch (Exception e) {
                etcdClient.setBrokenEtcdNode(etcdNode);
                LOGGER.warn("Lock etcd node [{}] broken", etcdNode);
            }
        }
    }
    
    /**
     * @方法名称 testBrokenEtcdNodes
     * @功能描述 失联节点 重连
     */
    private void testBrokenEtcdNodes() {
        List<String> brokenEtcdNodes = new ArrayList<String>(etcdClient.getBrokenEtcdNodes());
        for (String etcdNode : brokenEtcdNodes) {
            try {
                URI uri = new URI(etcdNode);
                testSocketConnect(uri.getHost(), uri.getPort());
                etcdClient.setAvailableEtcdNode(etcdNode);
                LOGGER.info("Lock etcd node [{}] available", etcdNode);
            } catch (Exception e) {
            }
        }
    }
    
    /**
     * @方法名称 testSocketConnect
     * @功能描述 心跳链接
     * @param host ip
     * @param port 端口
     * @throws IOException 链接异常
     */
    private void testSocketConnect(String host, int port)
        throws IOException {
        Socket socket = new Socket();
        socket.connect(new InetSocketAddress(host, port), 100);
        socket.close();
    }
    
}
