package com.nx.arch.addon.lock;

import com.nx.arch.addon.lock.lock.EtcdSolution;
import com.nx.arch.addon.lock.lock.RedisSolution;

/**
 * @类名称 NxLockFactory.java
 * @类描述 锁工厂
 * @作者  庄梦蝶殇 linhuaichuan@naixuejiaoyu.com
 * @创建时间 2020年3月28日 下午4:16:23
 * @版本 1.0.0
 *
 * @修改记录
 * <pre>
 *     版本                       修改人 		修改日期 		 修改内容描述
 *     ----------------------------------------------
 *     1.0.0 		庄梦蝶殇 	2020年3月28日             
 *     ----------------------------------------------
 * </pre>
 */
public class NxLockFactory {
    
    NxLockFactory() {
    }
    
    /**
     * 获取etcd锁方案对象
     */
    public EtcdSolution etcdSolution() {
        return new EtcdSolution();
    }
    
    /**
     * 获取redis锁方案对象
     */
    public RedisSolution redisSolution() {
        return new RedisSolution();
    }
}
